import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ElementUI from 'element-ui';
import '@/assets/them/index.css';

Vue.use(VueRouter)
Vue.use(ElementUI);


const routes = [
  {
    path: '/',
    name: 'home',
    redirect:{name:'manage'},
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/LoginView.vue')
  },
  {
    path: '/manage',
    name: 'manage',
    component: () => import('@/views/ManageView.vue'),
    children:[
      {
        path: 'store',
        name: 'store',
        component: () => import('@/views/StoreView.vue')
      },
      {
        path: 'category',
        name: 'category',
        component: () => import('@/views/CategoryView.vue')
      },
      {
        path: 'commodity',
        name: 'commodity',
        component: () => import('@/views/CommodityView.vue')
      },
      {
        path: 'coupon',
        name: 'coupon',
        component: () => import('@/views/CouponView.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
