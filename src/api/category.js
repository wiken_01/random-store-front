export default{
    GET_CATEGORY_LIST_BY_USERID: 'storeservice/storecategories/getStoreCategoriesListByUserId',
    UPDATE_CATEGORY: 'storeservice/storecategories/updateStoreCategories',
    DELETE_CATEGORY: 'storeservice/storecategories/delStoreCategories',
    ADD_CATEGORY:'storeservice/storecategories/addStoreCategories',
}