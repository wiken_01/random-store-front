export default{
    GET_STORE_LIST: '/storeservice/store/getStores',
    "ADD_STORE": '/storeservice/store/addStore',
    "UPDATE_STORE": '/storeservice/store/updateStore',
    "DELETE_STORE": '/storeservice/store/delStore'
}