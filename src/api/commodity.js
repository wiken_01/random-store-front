export default{
    getCommodityList: '/storeservice/commodity/getCommodityListByUserId',
    addCommodity: '/storeservice/commodity/addCommodity',
    updateCommodity: '/storeservice/commodity/updateCommodity',
    deleteCommodity: '/storeservice/commodity/delCommodity'
}