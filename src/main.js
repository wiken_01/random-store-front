import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from '@/utils/axios/axios'

import useMessage from '@/utils/message'

Vue.prototype.$axios = axios

Vue.config.productionTip = false

useMessage()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
