export default function DateFormatToString(date, format){
    //2023-02-20 04:00:00

    
    let month = (date.getMonth()+1) / 10 < 1 ? '0'+(date.getMonth()+1) : date.getMonth()+1
    let date_ = date.getDate() / 10 < 1 ? '0'+date.getDate() : date.getDate()
    let hours = date.getHours() / 10 < 1 ? '0'+date.getHours() : date.getHours()
    let minutes = date.getMinutes() / 10 < 1 ? '0'+date.getMinutes() : date.getMinutes()
    let seconds = date.getSeconds() / 10 < 1 ? '0'+date.getSeconds() : date.getSeconds()

    return `${date.getFullYear()}-${month}-${date_} ${hours}:${minutes}:${seconds}`
}