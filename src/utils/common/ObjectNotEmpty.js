
export default function(data, whiteList){
    let keys = Object.keys(data)

    for(let i = 0; i < keys.length; i++){
        let key = keys[i]

        let hasWhiteKey = false

        if(whiteList)
        for(let j = 0; j < whiteList.length; j++){
            if(key == whiteList[j]) hasWhiteKey = true
        }

        if(hasWhiteKey) continue

        if(data[key] === '' || data[key] == null){
            console.log(key, ' is empty')
            return false
        }

    }

    return true
}
