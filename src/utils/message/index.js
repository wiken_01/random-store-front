import Vue from 'vue'

export default function UseMessage(){
    Vue.prototype.$success = function (mes) {
        Vue.prototype.$message({
            message: mes,
            type: 'success'
        })
    }

    Vue.prototype.$error = function (mes) {
        Vue.prototype.$message.error({
            message: mes,
            type: 'error'
        })
    }

    Vue.prototype.$warning = function (mes) {
        Vue.prototype.$message.error({
            message: mes,
            type: 'warning'
        })
    }
}


