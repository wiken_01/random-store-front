import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    location
  }
})

// const location = {
//   state: () => ({
//     longitude: null,
//     latitude: null,
//   }),
//   mutations: {
//     setLocation(state, data) {
//       if(data.longitude)  state.longitude = data.longitude

//       if(data.latitude) state.longitude = data.latitude
//     }
//   },
//   actions: {

//   }
// }
