# random-store-front

## 项目介绍

本项目是探好店的店铺管理项目，能够管理店铺、店铺分类、商品、优惠劵。

店铺管理：https://gitee.com/wiken_01/random-store-front

用户端：https://gitee.com/wiken_01/random-front

管理员：https://gitee.com/wiken_01/random-admin-frontr

后端项目：https://gitee.com/wiken_01/random

### 店铺管理

![image-20230604153459212](./assets/image-20230604153459212.png)

![image-20230604153656837](./assets/image-20230604153656837.png)

### 店铺分类管理

![image-20230604153513041](./assets/image-20230604153513041.png)

![image-20230604153624407](./assets/image-20230604153624407.png)

### 商品管理

![image-20230604153531580](./assets/image-20230604153531580.png)

![image-20230604153606591](./assets/image-20230604153606591.png)

### 优惠劵管理

![image-20230604153546532](./assets/image-20230604153546532.png)

![image-20230604153553992](./assets/image-20230604153553992.png)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
